# Am I ready for a PhD?
A set of core reasons on why doing a PhD is exciting and important is listed. These materials are selected out of [this](https://www.cs.cmu.edu/~harchol/gradschooltalk.pdf) article from Carnegie Mellon University.

### 1. Depth
A Ph.D. is a long, in depth research exploration of one topic. By long we’re typically talking about 6 years. By in depth we mean that at the end of the Ph.D. you
will be the world expert or close to it in your particular area. You will know more than your advisor about your particular research area. You will know about your
research than anyone at your school. By one we mean that by the last couple years of your Ph.D., you will typically be working on only one narrow problem. The
Ph.D. is not about breadth, it is about depth.

### 2. Where is your focus?
There is no emphasis on courses in a PhD, but rather **research** becomes quite important.

### 3. Cannot rush for answers!
Please try not to rush for answers to your questions. Answers are not available **right away** in a PhD. Your ability to imagine, implement and push the limits will bring you the answer eventually.

### 4. Loneliness
You may be working with at most one students in your reserach lab. So, unlike your MSc or BSc classes that almost always the emphasis is on the joint and group work (especially on projects and assignments), there is not such criteria in a PhD. You have to be **Mentally Ready** for a PhD. A **Mental Readiness** is appreciated if you are **Self-Motivated** and **Pro-active**. That's the only thing you need to deal with loneliness during a PhD :)

### 5. With Bigger Ones.
During a PhD you will work **Side by Side** to your advisor. You both **learn from each other**. Sometimes watching the excitement and that **loud thaughts** coming out of your advisor becomes quite interesting and exciting (compared with the **dark side :)** that is shown during the boring lectures).


